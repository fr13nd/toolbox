#!/bin/bash
#Install tools from the tool.lst file in the same directory using apt
currentDir=$(pwd)
apt_tools="$currentDir/apt.tools"
go_tools="$currentDir/go.tools"
git_tools="$currentDir/git.tools"
scriptInfo="$currentDir/installStatus.log"
goExists=false
gitExists=false

#Do apt update
apt-get update

echo "[#] apt tools" | tee -a $scriptInfo
echo "[#] Installing apt tools" | tee -a $scriptInfo
#Install tools from apt
while read -r aptTool
do
	apt-get install $aptTool -y
	echo "[+] Tool installed: $aptTool"  | tee -a $scriptInfo
done < "$apt_tools"

echo "" | tee -a $scriptInfo

#Check if golang binary exists
prog="go"
var=$(which $prog)

if [ -z "$var" ]; then
	echo "[!] $prog doesn't exist" | tee -a $scriptInfo
else
	echo "[#] $prog exists" | tee -a $scriptInfo
	goExists=true
fi

#Install and build tools with go
if [ "$goExists" = true ]; then
	echo "[#] Installing go tools" | tee -a $scriptInfo
	while read -r goTool
	do
	go get $goTool
	echo "[+] go get $goTool" | tee -a $scriptInfo
	done < "$go_tools"
else
	echo "[!] go binary doesn't exist" | tee -a $scriptInfo
fi

echo ""  | tee -a $scriptInfo

#Check if git binary exists
prog="git"
var=$(which $prog)

if [ -z "$var" ]; then 
	echo "[!] $prog doesn't exist" | tee -a $scriptInfo
else
	echo "[#] $prog exists" | tee -a $scriptInfo
        gitExists=true
fi


#Clone tools from github
if [ "$gitExists" = true ]; then
	echo "[#] Installing git tools" | tee -a $scriptInfo
	while read -r gitTool
	do
		cd /opt
		git clone $gitTool
		echo "[+] git clone $gitTool" | tee -a $scriptInfo
	done < "$git_tools"
else
	echo "[!] git binary doesn't exist" | tee -a $scriptInfo
fi

echo "" | tee -a $scriptInfo

#Install stegsolve
stegDir="/opt/stegsolve"
if [ ! -d "$stegDir" ]; then
	echo "[+] Installing stegsolve" | tee -a $scriptInfo
	mkdir /opt/stegsolve
	cd /opt/stegsolve
	curl "https://raw.githubusercontent.com/zardus/ctf-tools/master/stegsolve/install" | bash
else
	echo "[!] $stegDir exists already" | tee -a $scriptInfo
fi

#Install socat binary
socatDir="/opt/socat"
if [ ! -d "$socatDir" ]; then
	echo "[+] Installing socat" | tee -a $scriptInfo
	mkdir /opt/socat
	cd /opt/socat
	wget https://github.com/andrew-d/static-binaries/raw/master/binaries/linux/x86_64/socat
else
	echo "[!] $socatDir exists already" | tee -a $scriptInfo
fi

#Install Reconnoitre
recDir="/opt/Reconnoitre"
if [ ! -d "$recDir" ]; then
	echo "[+] Installing reconnoitre" | tee -a $scriptInfo
	cd /opt
	git clone https://github.com/codingo/Reconnoitre.git
	cd Reconnoitre
	python setup.py install
else
	echo "[!] $recDir exists already" | tee -a $scriptInfo
fi

#Install linuxprivchecker.py
linPrivDir="/opt/linuxPrivChecker"
if [ ! -d "$linPrivDir" ]; then
	echo "[+] Installing linuxPrivChecker" | tee -a $scriptInfo
	mkdir "$linPrivDir"
	cd "$linPrivDir"
	wget https://www.securitysift.com/download/linuxprivchecker.py
else
	echo "[!] $linPrivDir exists already" | tee -a $scriptInfo
fi


echo ""
echo ""
echo "Install Status log"
echo "____________________________________________________________________"
echo ""
cat $scriptInfo
echo ""
echo "____________________________________________________________________"
